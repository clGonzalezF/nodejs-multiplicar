const { argv } = require('./config/yargs');
const { crearTablaEnArchivo, listarTabla } = require('./multiplicar/multiplicar');
const colors = require('colors');

let comando = argv._[0];

switch (comando) {
    case 'listar':
        listarTabla(argv.base, argv.limite);
        break;
    case 'crear':
        crearTablaEnArchivo(argv.base, argv.limite)
            .then(archivo => console.log('Archivo creado: ' + colors.bgGreen(archivo)))
            .catch(e => { console.log(e); });
        break;
    default:
        console.log('>> Comando no reconocido');
}