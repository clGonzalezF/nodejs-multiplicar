const yargs = require('yargs');

const conf = {
    base: {
        demand: true,
        alias: 'b',
        describe: 'numero'
    },
    limite: {
        default: 10,
        alias: 'l',
        describe: 'numero'
    }
}

let argv = yargs
    .command('listar', 'Lista la tabla de multiplicar', conf)
    .command('crear', 'Crea la tabla de multiplicar', conf)
    .help().argv;

console.log(argv);

module.exports = {
    argv
}