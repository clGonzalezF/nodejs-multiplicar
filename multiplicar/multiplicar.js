const fs = require('fs');
const colors = require('colors');

let listarTabla = (base, limite = 10) => {
    if (!Number(base)) {
        console.log(`El valor ingresado "${ base }" NO es un numero`);
        return;
    }

    console.log('===================='.yellow);
    console.log(`=== Tabla de ${ base } desde 1 hasta ${ limite } ===`.red);

    for (let i = 1; i <= limite; i++) {
        console.log(`${ base } * ${ i } = ${ base*i }`);
    }

    console.log('===================='.yellow);
}

let crearTablaEnArchivo = (base, limite = 10) => {
    return new Promise((resolve, reject) => {
        if (!Number(base)) {
            reject(`El valor ingresado "${ base }" NO es un numero`);
            return;
        }

        let data = '';

        for (let i = 1; i <= limite; i++) {
            data += `${ base } * ${ i } = ${ base*i }\n`;
        }

        fs.writeFile(`tablas/tabla-${ base }.txt`, data, (err) => {
            if (err) reject(err);
            else resolve(`tablas/tabla-${ base }.txt`);
        });
    });
}

module.exports = {
    crearTablaEnArchivo,
    listarTabla
}